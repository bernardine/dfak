---
layout: page
title: Signal
author: mfc
language: ru
summary: Способы связи
date: 2018-09
permalink: /ru/contact-methods/signal.md
parent: /ru/
published: true
---

Если вы используете мессенджер Signal, значит, ваши сообщения зашифрованы: только получатель может расшифровать и прочесть их. О самом факте связи тоже известно только вашему собеседнику. Имейте в виду, что в качестве имени пользователя Signal применяет телефонный номер, так что вы фактически раскрываете его своим собеседникам.

Что почитать: [Руководство по Signal для Android](https://ssd.eff.org/ru/module/%D1%80%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-%D0%BF%D0%BE-signal-%D0%B4%D0%BB%D1%8F-android), [How to: Use Signal for iOS](https://ssd.eff.org/en/module/how-use-signal-ios), [Как использовать Signal и не раскрыть свой телефонный номер](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
