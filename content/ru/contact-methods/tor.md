---
layout: page
title: Tor
author: mfc
language: ru
summary: Contact methods
date: 2018-09
permalink: /ru/contact-methods/tor.md
parent: /ru/
published: true
---

Tor Browser ориентирован на обеспечение приватности. Он позволяет подключаться к веб-сайтам анонимно и не раскрывает ваше местонахождение по IP-адресу, когда вы заходите на сайт.

Что почитать: [Обзор Tor](https://www.torproject.org/about/overview.html.en).
