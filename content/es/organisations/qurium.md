---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: 8AM-18PM Mon-Sun CET
response_time: 4 hours
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation es un Proveedor de Soluciones de Seguridad para medios independientes, organizaciones de derechos humanos, periodistas de investigación y activistas. Qurium ofrece un portfolio de soluciones profesionales, personalizadas y seguras, con apoyo personal a organizaciones e individuos en riesgo, lo que incluye:

- Alojamiento Seguro con mitigación DDoS para sitios web en riesgo
- Apoyo de Respuesta Rápida para organizaciones e individuos bajo amenaza inmediata
- Auditorías de seguridad para servicios web y aplicaciones móviles
- Elusión del bloqueo de sitios web en Internet
- Investigaciones forenses de ataques digitales, aplicaciones fraudulentas, malware dirigido y desinformación
