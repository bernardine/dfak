---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FLD.png
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: Emergencias 24/7, global; trabajo regular de Lunes a Viernes en horario de oficina, IST (UTC+1), el personal está basado en diferentes zonas horarias en diferenets regiones
response_time: El mismo día o el siguiente en caso de emergencia
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 para eergencias; +353-1-212-3750 teléfono de oficina
skype: front-line-emergency?call
email: info@frontlinedefenders.org para preguntas ol comentarios
initial_intake: yes
---

Front Line Defenders es una organización internacional basada en Irlanda que trabaja con la protección integral de defensores de derechos humanos en riesgo. Front Line Defenders provee soporte rapido y práctico a defensores de derechos humanos en riesgo inmediato a través de subvenciones de seguridad, entrenamientos en seguridad física y digital, incidencia y campañas.

Front Line Defenders opera una línea de ayuda de emergencia 24/7 en +353-121-00489 para defensores de derechos humanos en riesgo inmediato en Árabe, Inglés, Francés, Ruso o Español. Cuando los defensores de derechos humanos enfrentan riesgos inmediatos sobre sus vidas, Front Line Defenders puede ayudar con relocalización temporal. Front Line Defenders provee entrenamiento en seguridad física y seguridad digital. Front Line Defenders también publica los casos de defensores de derechos humanos en riesgo y hace campañas e incidencia a nivel internacional incluyendo la Unión Europea, Naciones Unidas, mecanismos interregionales y directamente con gobiernos por la protección que puedan brindar.
