---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 12 horas
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Complementario a su trabajo principal, Internews también trabaja con particulares, organizaciones y comunidades alrededor del mundo para aumentar la concientización sobre seguridad digital, proteger el acceso a una internet abierta y sin censura y mejorar las prácticas en seguridad digital. Internews ha capacitado a miles de periodistas y defensores de los derechos humanos en más de 80 países, y tiene redes de entrenadores de seguridad digital y auditores familiarizados con el marco de trabajo SAFETAG (Security Auditing Framework and Evaluation Template for Advocacy Groups), cuyo desarrollo ha sido impulsado por Internews. Esta organización también está construyendo alianzas tanto con actores de la sociedad civil como con firmas privadas de inteligencia de riesgo y análisis, pudiendo apoyarlos directamente para mantener una presencia online segura y sin censura. Internews ofrece intervenciones técnicas y no técnicas por igual que van desde evaluaciones de seguridad usando el marco de trabajo SAFETAG hasta estrategias revisadas de mitigación basadas en la investigación de amenazas. Internews trabaja apoyando directamente análisis de phishing y malware para grupos trabajando en derechos humanos y medios que experimentan ataques digitales dirigidos.
