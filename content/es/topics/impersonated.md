---
layout: page
title: "Alguien me está suplantando en línea"
author: Floriana Pagano, Alexandra Hache
language: es
summary: "Alguien me está suplantado a través de una cuenta de redes sociales, una dirección de correo electrónico, una llave PGP, un sitio web o una aplicación"
date: 2019-08
permalink: /es/topics/impersonated
parent: /es/
---

# Alguien me está suplantando en línea

Una amenaza que enfrentan muchos activistas, defensores de los derechos humanos, ONGs, medios independientes y blogueros es la suplantación de identidad por parte adversarios que crean perfiles falsos, sitios web o correos electrónicos con sus nombres. En ocasiones, se tratan de crear campañas de difamación, información engañosa, ingeniería social o robo de identidad para generar ruido, problemas de confianza y violaciones de datos que impactan en la reputación de las personas y colectivos suplantados. Este es un problema frustrante que puede afectar en diferentes niveles la capacidad de las víctimas para comunicarse e informar, y puede tener diferentes causas dependiendo de dónde y cómo se les esté personificando.

Es importante saber que hay muchas formas de hacerse pasar por otra persona (perfiles falsos en las redes sociales, sitios web clonados, correos electrónicos falsificados, publicaciones no consensuales de imágenes y videos personales, etc.). Las estrategias pueden variar desde la solicitud de bajada de información, la prueba de propiedad original, la reclamación de los derechos de autor del sitio web original o su información, o la advertencia a sus redes y contactos personales a través de comunicaciones públicas o confidenciales. Diagnosticar el problema y encontrar posibles soluciones para la suplantación de identidad puede ser complicado. A veces será casi imposible presionar a una pequeña empresa de hosting para que elimine un sitio web, y es posible que se requieran acciones legales. Es una buena práctica configurar alertas y monitorear Internet para averiguar si tu o tu organización están siendo suplantados.

Esta sección del kit de primeros auxilios digitales te guiará a través de algunos pasos básicos para diagnosticar posibles formas de suplantación y estrategias de mitigación para eliminar cuentas, sitios web y correos electrónicos que se hacen pasar por ti o tu organización.

Si estás siendo suplantado, sigue este cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.


## Workflow

### urgent_question

¿Temes por tu integridad física o bienestar?

  - [Sí](#physical_sec_end)
  - [No](#diagnostic_start1)

### diagnostic_start1

¿La suplantación de identidad te afecta como persona (alguien está usando tu nombre y apellido legal, o el apodo en el que basa tu reputación) o como organización/colectivo?

- [Como individuo](#individual)
- [Como organización](#organization)

### individual

> Si estás siendo afectado como individuo, es posible que desees alertar a tus contactos. Realiza este paso utilizando una cuenta de correo, un perfil o un sitio web que esté totalmente bajo tu control.

- Una vez que hayas informado a tus contactos que estás siendo suplantado, continúa con el [próximo paso](#diagnostic_start2).

### organization

> Si estás siendo afectado como grupo, es posible que desees hacer un comunicado público. Toma este paso utilizando una cuenta de correo, un perfil o un sitio web que esté totalmente bajo tu control.

- Una vez que hayas informado a tu comunidad que estás siendo suplantado, continúa con el [siguiente paso](#diagnostic_start2).

### diagnostic_start2

¿Cómo estás siendo suplantado?

- [A través de un sitio web falso se hace pasar por mi persona o mi grupo](#fake_website)
- [A través de una cuenta de red social](#social_network)
- [A través de la distribución de videos o imágenes sin consentimiento](#other_website)
- [A través de mi dirección de correo electrónico o una dirección similar](#spoofed_email1)
- [A través de una llave PGP conectada a mi dirección de correo electrónico](#PGP)
- [A través de una aplicación falsa que imita mi aplicación](#app1)

### social_network

¿En qué plataforma de redes sociales estás siendo personificado?

- [Facebook](#facebook)
- [Twitter](#twitter)
- [Google](#Google)
- [Instagram](#instagram)

###  Facebook

> Sigue [estas instrucciones](https://www.facebook.com/help/174210519303259) para solicitar la eliminación de la cuenta que está realizando suplantación.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Funcionó?

- [Sí](#resolved_end)
- [No](#account_end)

### twitter

> Rellena [este formulario](https://help.twitter.com/forms/impersonation) para solicitar la eliminación de la cuenta de suplantación.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#account_end)

### google

> Rellena [este formulario](https://support.google.com/plus/troubleshooter/1715140) para solicitar la eliminación de la cuenta de suplantación.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#account_end)

### instagram

> Sigue [estas instrucciones](https://help.instagram.com/446663175382270) para solicitar la eliminación de la cuenta de suplantación.
>
> Ten en cuenta que se puede tardar algún tiempo en recibir una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#account_end)


### fake_website

> Comprueba si este sitio web se conoce como malicioso buscando su URL en los siguientes servicios en línea:
>
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)
> - [ThreatCrowd](https://www.threatcrowd.org/)

¿Se sabe que el dominio es malicioso?

  - [Sí](#malware_website)
  - [No](#non-malicious_website)

### malware_website

> Reporta la URL a Google Safe Browsing completando [este formulario (En Inglés)](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Ten en cuenta que puede llevar algún tiempo asegurarse de que tu informe sea procesado. Mientras tanto, puedes continuar con el siguiente paso para enviar una solicitud de eliminación al proveedor de hosting y al registrador de dominios, o guardar esta página en tus marcadores y regresar en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#non-malicious_website)


### non-malicious_website

> Puedes intentar reportar el sitio web al proveedor de hosting o al registrador de dominios, solicitando su eliminación.
>
> Si el sitio web que deseas reportar está utilizando tu contenido, es posible que tengas que probar que eres el propietario legítimo del contenido original. Puedes mostrar esto presentando tu contrato original con el registrador de dominios y/o el proveedor de hosting, pero también puede hacer una búsqueda en [Wayback Machine](https://archive.org/web/), buscando la URL de tu sitio web y el sitio web falso. Si los sitios web se han indexado allí, encontrarás un historial que puede permitirte demostrar que tu sitio web existía antes de que se publicara el sitio web falso.
>
> Para enviar una solicitud de eliminación, también deberás recopilar información sobre el sitio web falso:
>
> - Ve a [este sitio web](https://network-tools.com/nslookup/) y descubre la dirección IP (o direcciones) del sitio web falso ingresando su URL en el formulario de búsqueda.
> - Anota la dirección o direcciones IP.
> - Ve a [este sitio web](https://whois.domaintools.com/) y busca tanto el dominio como las direcciones IP del sitio web falso.
> - Toma nota del nombre y la dirección de correo electrónico de abuso del proveedor de alojamiento y del servicio de dominio. Si se incluye en los resultados de su búsqueda, también toma nota del nombre del propietario del sitio web.
> - Escribe al proveedor de hosting y al registrador de dominios del sitio web falso para solicitar su eliminación. En el mensaje, incluye información sobre las direcciones IP, la URL y el propietario del sitio web que realiza suplantación de identidad, así como las razones por las que es abusivo.
> - Puedes usar [esta plantilla (En Inglés)](https://accessnowhelpline.gitlab.io/community-documentation/352-Report_Fake_Domain_Hosting_Provider.html) para escribir al proveedor de alojamiento.
> - Puedes usar [esta plantilla (En Inglés)](https://accessnowhelpline.gitlab.io/community-documentation/343-Report_Domain_Impersonation_Cloning.html) para escribir al registrador del dominio.
>
> Ten en cuenta que puede tardar un tiempo recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha funcionado?

- [Sí](#resolved_end)
- [No](#web_protection_end)


### spoofed_email1

> Por razones técnicas propias de esta tecnología, es bastante difícil autenticar correos electrónicos. Esta es la razón por la cual es muy fácil crear direcciones de remitentes falsificadas y correos electrónicos falsos.

¿Estás siendo suplantado a través de tu dirección de correo electrónico o similar, por ejemplo, con el mismo nombre de usuario, pero con un dominio diferente?

- [Estoy siendo suplantado a través de mi dirección de correo electrónico](#spoofed_email2)
- [Estoy siendo suplantado a través de una dirección de correo electrónico similar](#similar_email)


### spoofed_email2

> La persona que se está haciendo pasar por ti podría haber pirateado tu cuenta de correo electrónico. Para descartar esta posibilidad, intenta cambiar tu contraseña.

¿Pudiste cambiar tu contraseña?

- [Sí](#spoofed_email3)
- [No](#hacked_account)

### hacked_account

Si no puedes cambiar tu contraseña, tu cuenta de correo electrónico probablemente esté comprometida.

- Puedes seguir [este cuestionario](../../../account-access-issues) para resolver este problema.

### spoofed_email3

> La suplantación de correo electrónico consiste en enviar mensajes con una dirección de remitente falsificada. El mensaje parece haberse originado de alguien o en otro lugar que no es la fuente real.
>
> La suplantación de correos electrónicos es común en las campañas de phishing y correo basura (spam), ya que las personas tienen más probabilidades de abrir un correo electrónico cuando creen que proviene de una fuente legítima.
>
> Si alguien está falsificando tu correo electrónico, debes informar a tus contactos para advertirles sobre el peligro de este ataque de phishing (házlo desde una cuenta de correo, perfil o sitio web que esté totalmente bajo tu control).
>
> Si crees que esta suplantación de identidad esta relacionada a campañas de phishing u otros intentos maliciosos, también puedes leer la sección [Recibí mensajes sospechosos](../../../suspicious_messages).

¿Se detuvieron los correos electrónicos después de cambiar la contraseña de tu cuenta de correo electrónico?

- [Sí](#compromised_account)
- [No](#secure_comms_end)


### compromised_account

> Probablemente tu cuenta haya sido hackeada por alguien que la usó para enviar correos electrónicos haciéndose pasar por ti. Como tu cuenta se vio comprometida, es posible que también desees leer la sección [Perdí acceso a mis cuentas](../../../account-access-issues).

¿Esto ha ayudado a resolver tu problema?

- [Sí](#resolved_end)
- [No](#account_end)


### similar_email

> Si el imitador está utilizando una dirección de correo electrónico que es similar a la tuya pero con un nombre de dominio o usuario diferente, es una buena idea advertir a tus contactos sobre este intento (házlo desde una cuenta de correo, perfil o sitio web totalmente bajo tu control).
>
> También es posible que desees leer la sección [Recibí mensajes sospechosos](../../../suspicious-messages), ya que esta suplantación podría estar relacionada a ataques de phishing.

¿Esto ha ayudado a resolver tu problema?

- [Sí](#resolved_end)
- [No](#secure_comms_end)


### PGP

¿Crees que tu llave PGP privada podría haber sido comprometida, por ejemplo, porque perdiste el control del dispositivo donde estaba almacenada?

- [Sí](#PGP_compromised)
- [No](#PGP_spoofed)

### PGP_compromised

¿Todavía tienes acceso a tu llave privada?

- [Sí](#access_to_PGP)
- [No](#lost_PGP)

### access_to_PGP

> - Revoca tu llave.
> - [Instrucciones para Enigmail (En Inglés)](https://www.enigmail.net/documentation/Key_Management#Revoking_your_key_pair)
> - Crea un nuevo par de llaves y haz que lo firmen personas de confianza.
> - Comunicándote a través de un canal que controles, informa a tus contactos que revocaste tu llave y generaste una nueva.

¿Necesitas más ayuda para resolver tu problema?

- [Sí](#secure_comms_end)
- [No](#resolved_end)


### lost_PGP

¿Tienes un certificado de revocación?

- [Sí](#access_to_PGP)
- [No](#no_revocation_cert)


### no_revocation_cert

> - Crea un nuevo par de llaves y haz que lo firmen personas de confianza.
> - Informa a tus contactos a través de un canal que controles, que deben usar tu nueva llave y dejar de usar la anterior.

¿Necesitas más ayuda para resolver tu problema?

- [Sí](#secure_comms_end)
- [No](#resolved_end)

### PGP_spoofed

¿Está tu llave firmada por personas de confianza?

- [Sí](#signed_key)
- [No](#non-signed_key)

### signed_key

> Informa a tus contactos a través de un canal que controles que alguien está tratando de hacerse pasar por ti y díles que pueden reconocer tu llave real en función de las firmas en tus mensajes.

¿Necesitas más ayuda para resolver tu problema?

- [Sí](#secure_comms_end)
- [No](#resolved_end)

### non-signed_key

> - Haz que tu llave sea firmada por personas de confianza.
> - Informa a tus contactos a través de un canal que controles que alguien está tratando de hacerse pasar por ti y díles que pueden reconocer tu llave real en función de las firmas en tus mensajes.

¿Necesitas más ayuda para resolver tu problema?

- [Sí](#secure_comms_end)
- [No](#resolved_end)

### other_website

> Si estás siendo suplantado en un sitio web, lo primero que debes hacer es comprender en dónde está alojado ese sitio, quién lo administra y quién le ha proporcionado el nombre de dominio. Esta investigación tiene como objetivo identificar la mejor manera de solicitar la eliminación del contenido malicioso.
>
> Antes de continuar con tu investigación, si eres ciudadano de la UE, puedes solicitar a Google que elimine este sitio web de sus resultados de búsqueda en tu nombre.

¿Eres ciudadano de la Unión Europea?

- [Sí](#EU_privacy_removal)
- [No](#doxing_question)


### EU_privacy_removal

> Rellena [este formulario](https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&hl=es&rd=1&pli=1) para eliminar el sitio web de los resultados de búsqueda de Google en tu nombre.
>
> Lo que necesitarás:
>
> - Una copia digital de un documento de identificación (si estás enviando esta solicitud en nombre de otra persona, deberás proporcionar la documentación de identificación de esta).
> - La(s) URL(s) del contenido que contiene la información personal que se desea eliminar
> - Para cada URL que proporciones, deberás explicar:
> 1. Cómo la información personal identificada anteriormente se relaciona con la persona en cuyo nombre se realiza la solicitud
> 2. Por qué crees que la información personal debe ser eliminada
>
> Ten en cuenta que si has iniciado sesión en tu cuenta de Google, Google puede asociar el envío del formulario con esta cuenta.
>
> Después de enviar el formulario, tendrás que esperar una respuesta de Google para verificar que se hayan eliminado los resultados.

¿Deseas presentar una solicitud de eliminación para remover el contenido suplantado del sitio web?

- [Sí](#doxing_question)
- [No, me gustaría recibir asistencia](#account_end)

### doxing_question

¿El suplantador ha publicado información personal, videos íntimos o imágenes tuyas?

- [Sí](../../../harassed-online/questions/doxing_web)
- [No](#fake_website)

### app1

> Si alguien está difundiendo una copia maliciosa de tu aplicación u otro software, es una buena idea hacer un comunicado público para advertir a los usuarios que solo descarguen la versión legítima.
>
> También debes informar sobre la aplicación maliciosa y solicitar su eliminación.

¿En dónde se distribuye la copia maliciosa de su aplicación?

- [En Github](#github)
- [En Gitlab.com](#gitlab)
- [En Google Play Store](#playstore)
- [En Apple App Store](#apple_store)
- [En otro sitio web](#fake_website)

### github

> Si el software malicioso está alojado en Github, lea [esta guía (En Inglés)](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) para eliminar contenido que viola derechos de autor.
>
> Puede tardar un tiempo mientras llega una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha ayudado a resolver su problema?

- [Sí](#resolved_end)
- [No](#app_end)

### gitlab

> Si el software malicioso está alojado en Gitlab.com, lea [esta guía (En Inglés)](https://about.gitlab.com/handbook/dmca/) para eliminar contenido que viola derechos de autor.
>
> Puede tardar [un tiempo](https://about.gitlab.com/handbook/support/workflows/services/gitlab_com/dmca.html) mientras llega una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha ayudado a resolver su problema?

- [Sí](#resolved_end)
- [No](#app_end)


### playstore

> Si la aplicación maliciosa está alojada en Google Play Store, sigue [estas instrucciones](https://support.google.com/legal/troubleshooter/1114905) para eliminar contenido que viola  derechos de autor.
>
> Puede tardar un tiempo mientras llega una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve en unos días.

¿Esto ha ayudado a resolver su problema?

- [Sí](#resolved_end)
- [No](#app_end)


### apple_store

> Si la aplicación maliciosa está alojada en la App Store, sigue [estas instrucciones](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=es) para eliminar contenido que viola derechos de autor.
>
> Puede tardar un tiempo mientras llega una respuesta a tu solicitud. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Esto ha ayudado a resolver su problema?

- [Sí](#resolved_end)
- [No](#app_end)


### physical_sec_end

> Si estás temiendo por tu bienestar físico, comunícate con las organizaciones a continuación que pueden ayudarte.

:[](organisations?services=physical_sec)


### account_end

> Si aún experimentas suplantación de identidad o tu cuenta aún está comprometida, comunícate con las organizaciones a continuación que pueden ayudarte.

:[](organisations?services=account&services=legal)


### app_end

> Si la aplicación falsa no se ha eliminado, comunícate con las organizaciones a continuación que pueden ayudarte.

:[](organisations?services=account&services=legal)

### web_protection_end

> Si tus solicitudes de eliminación no han tenido éxito, puedes intentar comunicarte con las organizaciones a continuación para obtener más ayuda.

:[](organisations?services=web_protection)

### secure_comms_end

> Si necesitas ayuda o recomendaciones sobre phishing, seguridad y cifrado de correo electrónico y comunicaciones seguras en general, puedes comunicarte con estas organizaciones:

:[](organisations?services=secure_comms)


### resolved_end

Esperamos que esta guía de DFAK te haya sido útil. Envíanos tus comentarios [por correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

Para evitar nuevos intentos de hacerse pasar por ti lee los consejos a continuación.

### final_tips

- Crea contraseñas seguras, complejas y únicas para todas tus cuentas.
- Considera usar un administrador de contraseñas para crear y almacenar contraseñas, de modo que puedas usar muchas contraseñas diferentes en diferentes sitios y servicios sin tener que memorizarlas.
- Activa la autenticación de dos factores (2FA) para tus cuentas más importantes. 2FA ofrece una mayor seguridad de la cuenta al requerir usar más de un método para iniciar sesión en sus cuentas. Esto significa que incluso si alguien obtuviera tu contraseña principal, no podría acceder a tu cuenta a menos que también tuviera tu teléfono móvil u otro medio secundario de autenticación.
- Verifica tus perfiles en plataformas de redes sociales. Algunas plataformas ofrecen una función para verificar tu identidad y vincularla a tu cuenta.
- Mapea tu presencia en línea. El self-doxing consiste en explorar la inteligencia de código abierto sobre uno mismo para evitar que los actores maliciosos encuentren y utilicen esta información para hacerse pasar por ti. En esencia investigarse uno mismo.
- Configura las alertas de Google. Puedes recibir correos electrónicos cuando aparezcan nuevos resultados para un tema en la Búsqueda de Google. Por ejemplo, puedes obtener información sobre las menciones de tu nombre o el nombre de tu organización / colectivo.
- Captura el contenido de tu página web como aparece ahora para usarla como evidencia en el futuro. Si tu sitio web permite robots exploradores o crawlers, puedes usar Wayback Machine, ofrecida por archive.org. Visita [esta página](https://archive.org/web/) y haga clic en el botón "Guardar página ahora".

#### resources

- [Access Now: Autenticación de dos factores o 2FA](https://www.accessnow.org/entendiendo-la-autenticacion-de-dos-factores/)
- [Access Now Helpline Community Documentation: Elegir un administrador de contraseñas - En Inglés](https://accessnowhelpline.gitlab.io/community-documentation/295-Password_managers.html)
- [Access Now Helpline Community Documentation: Una guía para evitar el doxing - En Inglés](https://guides.accessnow.org/self-doxing/self-doxing.html)
- [Archive.org: Archive su sitio web](https://archive.org/web/)
- [Security Self-Defense: Creando Contraseñas Seguras](https://ssd.eff.org/es/module/creando-contrase%C3%B1as-seguras)
- [Security Self-Defense: Visión animada: Uso de gestores de contraseñas para estar seguro en línea](https://ssd.eff.org/es/module/visi%C3%B3n-animada-uso-de-gestores-de-contrase%C3%B1as-para-estar-seguro-en-l%C3%ADnea)
- [Security Self-Defense: Cómo usar KeePassXC - un administrador de contraseñas de código abierto seguro](https://ssd.eff.org/es/module/c%C3%B3mo-usar-keepassxc)