---
layout: page
title: "¿Estás siendo acosado o acosada en línea?"
author: Floriana Pagano
language: es
summary: "¿Estás siendo acosado o acosada en línea?"
date: 2019-08
permalink: /es/topics/harassed-online/
parent: /es/

---

# ¿Estás siendo acosado o acosada en línea?

La Internet, y en particular las redes sociales, se han convertido en un espacio crítico para que los miembros y las organizaciones de la sociedad civil, especialmente las mujeres, las personas LGBTIQ y otras minorías, se expresen y hagan oír su voz. Pero al mismo tiempo, también se han convertido en espacios donde estos grupos son fácilmente señalados por expresar sus opiniones. La violencia y el abuso en línea les niegan a las mujeres, a las personas LGBTIQ y a muchas otras personas desprivilegiadas el derecho a expresarse por igual, libremente y sin temor.

La violencia y el abuso en línea tienen muchas formas diferentes, y los perpetradores a menudo dependen de la impunidad, también debido a la falta de leyes que protegen a las víctimas de acoso en muchos países, pero sobre todo porque las estrategias de protección deben ajustarse de manera creativa dependiendo de qué tipo de ataque se está ejecutando.

Por lo tanto, es importante identificar la tipología del ataque para decidir qué pasos podemos tomar.

Esta sección del kit de primeros auxilios digitales te guiará a través de algunos pasos básicos para planificar cómo protegerte contra el ataque que estás sufriendo.

Si eres blanco de acoso en línea, sigue este cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.

## Workflow

### physical_wellbeing

¿Temes por tu integridad física o bienestar?

- [Sí](#physical_risk_end)
- [No](#no_physical_risk)

### no_physical_risk

¿Crees que el atacante ha accedido o está accediendo a tu dispositivo?

 - [Sí](#device_compromised)
 - [No](#account_compromised)

### device_compromised

> Cambia la contraseña para acceder a tú dispositivo por otra contraseña única, larga y compleja:
>
> - [Mac OS](https://support.apple.com/es-lamr/HT202860)
> - [Windows](https://support.microsoft.com/es-us/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/es-lamr/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=es)

¿Se ha bloqueado efectivamente al atacante de tu dispositivo?

 - [Sí](#account_compromised)
 - [No](../../../device-acting-suspiciously)

### account_compromised

> Si alguien tuvo acceso a tu dispositivo, es posible que también hayan accedido a tus cuentas en línea, por lo que podría estar leyendo tus mensajes privados, identificando tus contactos y haciendo publicaciones de imágenes o videos haciéndose pasar por ti.

¿Has notado la desaparición de publicaciones o mensajes, u otras actividades que te den una buena razón para pensar que tu cuenta ha sido comprometida? Revisa también tus carpetas de mensajes enviados por posibles actividades sospechosas.

 - [Sí](../../../account-access-issues)
 - [No](#impersonation)

### impersonation

¿Alguien se está haciendo pasar por ti?

- [Sí](../../../impersonated)
- [No](#doxing)

### doxing

¿Alguien ha publicado información privada o fotos sin tu consentimiento?

- [Sí](#doxing_yes)
- [No](#hate_speech)

### doxing_yes

¿Dónde se ha publicado tu información privada o imágenes?

- [En una plataforma de redes sociales](#doxing_sn)
- [En un sitio web](#doxing_web)

### doxing_sn

> Si tu información privada o tus imágenes se han publicado en una plataforma de redes sociales, puedes reportar una violación de los estándares de la comunidad siguiendo los procedimientos de proporcionados a los usuarios por los sitios web de redes sociales. Encontrarás instrucciones para las plataformas principales en [este enlace](https://acoso.online/cl/reporta-el-caso-en-las-plataformas-de-internet/).

¿Se pudo eliminar la información o los contenidos multimedia?

 - [Sí](#one_more_persons)
 - [No](#harassment_end)

### doxing_web

> Sigue [estas instrucciones (En Inglés)](https://withoutmyconsent.org/resources/take-down) para bajar contenido de un sitio web.

¿El contenido ha sido retirado por el sitio web?

- [Sí](#one_more_persons)
- [No](#harassment_end)

### hate_speech

¿El ataque se basa en atributos como la raza, el género o la religión?

- [Sí](#one_more_persons)
- [No](#harassment_end)


### one_more_persons

¿Has sido atacado por una o más personas?

- [One person](#one_person)
- [More persons](#more_persons)

### one_person

¿Conoces a esta persona?

- [Sí](#known_harasser)
- [No](#block_harasser)


### block_harasser

>Si sabes quién te está acosando, puedes considerar informar a las autoridades de tu país. Cada país tiene leyes diferentes para proteger a las personas del acoso en línea, por lo que debes explorar la legislación en tu país para decidir qué hacer.
>
> Si decides demandar a esta persona, debes comunicarse con un experto legal.

¿Quieres demandar al atacante?

 - [Sí](#legal_end)
 - [No](#block_harasser)


### block_harasser

> Ya sea que sepas quién es tu acosador o no, siempre es una buena idea bloquearlos en las plataformas de redes sociales siempre que sea posible.
>
> - [Facebook](https://www.facebook.com/help/290450221052800)
> - [Twitter](https://help.twitter.com/es/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=es)
> - [Tumblr](https://tumblr.zendesk.com/hc/es/articles/231877648-Bloquear-a-otros-usuarios)
> - [Instagram](https://help.instagram.com/426700567389543)

¿Has bloqueado a tu acosador de manera efectiva?

 - [Sí](#resolved_end)
 - [No](#harassment_end)


### more_persons

> Si estás siendo atacado por más de una persona, podrías ser el objetivo de una campaña de acoso y deberás reflexionar sobre cuál es la mejor estrategia que se aplica a tu caso.
>
> Para aprender sobre todas las estrategias posibles, lee esta [página](https://www.takebackthetech.net/es/be-safe/discurso-de-odio-estrategias)

¿Has identificado la mejor estrategia para ti?

 - [Sí](#resolved_end)
 - [No](#harassment_end)

### harassment_end

> Si aún estás bajo acoso y necesitas una solución personalizada, por favor comunícate con las organizaciones descritas a continuación que pueden ayudarte.

:[](organisations?services=harassment)


### physical_risk_end

> Si estás en riesgo físico, comunícate con las organizaciones descritas a continuación que pueden ayudarte.

:[](organisations?services=physical_security)


### legal_end

> Si necesitas asistencia legal, comunícate con las organizaciones descritas a continuación que pueden ayudarte.

:[](organisations?services=legal)

### resolved_end

Esperamos que esta guía de solución de problemas te haya sido útil. Por favor danos tu opinión [vía email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Consejos_finales

- **Documenta el acoso:** Es útil documentar los ataques o cualquier otro incidente que puedas estar presenciando: toma capturas de pantalla, guarda los mensajes que recibas de los acosadores, etc. Si es posible, crea un diario donde puedas sistematizar esta documentación. fechas, horas, plataformas y espacios en línea, Identificadores de usuario, capturas de pantalla, descripción de lo que sucedió, etc. Este seguimiento puede ayudarte a detectar posibles patrones e indicaciones sobre tus posibles atacantes. Si te sientes abrumado o abrumada, trata de pensar en alguien de tu confianza que pueda documentar los incidentes por ti durante un tiempo. Debes confiar plenamente en la persona que administrará esta documentación, ya que deberás entregarle las credenciales de tus cuentas personales. Una vez que sientas que puedes recuperar el control de tus cuentas, recuerda cambiar tus contraseñas.

    - Puedes encontrar instrucciones sobre cómo documentar incidentes en [esta página](https://www.techsafety.org/sugerencias-sobre-la-documentacion-para-sobrevivientes-del-abuso-tecnolgico-y-acecho).

- **Configura la autenticación en 2 pasos** en todas tus cuentas. La autenticación en 2 pasos puede ser muy efectiva para evitar que alguien acceda a tus cuentas sin tu permiso. Si puedes elegir, no utilices la autenticación de 2 pasos basada en SMS y elige una opción diferente, basada en una aplicación de teléfono o en una clave de seguridad.

    - Si no sabes cuál es la mejor solución para ti, puedes consultar [esta infografia](https://www.accessnow.org/cms/assets/uploads/2017/12/2FA-infographic-ESP.png) y [este post  (En Inglés)](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - Puedes encontrar instrucciones para configurar la autenticación en 2 pasos en las plataformas principales [aquí (En Inglés)](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Mapea tu presencia en línea**. El auto doxing consiste en explorar la inteligencia de código abierto sobre nosotros mismos para evitar que actores maliciosos encuentren y utilicen esta información para hacerse pasar por ti.


#### Resources

- [Access Now Helpline Community Documentation: Guide to prevent doxing - En Inglés](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation: FAQ - Online Harassment Targeting a Civil Society member](https://accessnowhelpline.gitlab.io/community-documentation/234-FAQ-Online_Harassment.html)​​​​​​​
- [Acoso.Online](https://acoso.online)
- [Cloud seguro: ¿Qué hacer si soy víctima de Ciberacoso?](https://www.cloudseguro.co/victima-de-ciberacoso/)
- [Equality Labs: Guía anti-doxing para activistas que enfrentan ataques desde la Alt-derecha (En Inglés)](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Bloqueando tu identidad digital (En Inglés)](http://femtechnet.org/csov/lock-down-your-digital-identity/)
​​​​​​​- [National Network to End Domestic Violence: Sugerencias sobre la Documentación para Sobrevivientes del Abuso Tecnológico y Acecho](https://www.techsafety.org/sugerencias-sobre-la-documentacion-para-sobrevivientes-del-abuso-tecnolgico-y-acecho)
- [Stop bullying: Reportar casos de ciberacoso](https://espanol.stopbullying.gov/acoso-por-internet/c%C3%B3mo-denunciar-129k/%C3%ADndice.html)
