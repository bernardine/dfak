---
layout: page.pug
title: "Acerca de"
language: es
summary: "Acerca del Kit de primeros auxilios digitales DFAK."
date: 2019-03-13
permalink: /es/about/
parent: Home
---
El Kit de primeros auxilios digitales es un esfuerzo colaborativo de la [Red de respuesta rápida (RaReNet)](https://www.rarenet.org/) y de [CiviCERT](https://www.civicert.org/).

RaReNet es una red internacional de grupos que trabajan brindando respuesta rápida y organizaciones referencia en el campo de la seguridad digital como Access Now,  Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtual Road, así como expertos particulares que trabajan en el campo de la seguridad digital y la respuesta rápida.

Algunas de estas organizaciones y particulares son parte de CiviCERT, una red internacional de mesas de soporte en seguridad digital y proveedores de infraestructura que están enfocados principalmente en apoyar a grupos y organizaciones que trabajan por la justicia social y por la defensa de los derechos humanos y digitales. CiviCERT es una figura profesional para los esfuerzos distribuidos de CERTs (Equipos de respuesta de emergencias computacionales o Computer Emergency Response Team) en la comunidad. CiviCERT está acreditado por Trusted Introducer, la red europea de equipos de respuesta de emergencia computacional de confianza.

El Kit de primeros auxilios digitales también es un [proyecto de código abierto que acepta contribuciones externas](https://gitlab.com/rarenet/dfak).

Si desea utilizar el Kit de primeros auxilios digitales en contextos en los que la conectividad es limitada, o encontrar una conexión es difícil, puede descargar [una versión fuera de línea aquí](https://www.digitalfirstaid.org/dfak-offline.zip).

Para cualquier comentario, sugerencia o pregunta sobre el Kit de primeros auxilios digitales, puedes escribir a: dfak @ digitaldefenders . org

GPG - Huella digital: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
