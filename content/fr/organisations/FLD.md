---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: emergency 24/7, global; regular work Monday-Friday at office hours, IST (UTC+1), staff are located in various time zones in different regions
response_time: same or next day in case of emergency
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders est une organisation internationale basée en Irlande qui œuvre pour la protection intégrée des défenseurs des droits de l'homme soumis à des risques. 

Front Line Defenders fournit un soutien rapide et pratique aux défenseurs des droits humains en danger immédiat par le biais de subventions de sécurité, de formation en sécurité physique et numérique, de campagnes de soutien.

Front Line Defenders proposent une ligne d'assistance téléphonique d'urgence 24 heures sur 24, 7 jours sur 7 au +353-121-00489 pour les défenseurs des droits humains en danger immédiat en Arabe,
Anglais, Espagnol, Français ou Russe. 

Lorsque les défenseurs des droits humains font face à une menace immédiate pour leur vie, les défenseurs de Front Line peuvent les aider à déménager temporairement. 

Front Line Defenders offre une formation en sécurité physique et en sécurité numérique. 

Front Line Defenders rend public également des cas de défenseurs des droits humains en danger et mène des campagnes de soutien pour favoriser leur protection au niveau international, y compris au sein de l'UE, à l'ONU, à travers des mécanismes interrégionaux ou auprès de gouvernements directement.
