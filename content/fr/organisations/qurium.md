---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: 8AM-18PM Mon-Sun CET
response_time: 4 hours
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation est un fournisseur de solutions de sécurité pour les médias indépendants, les organisations de défense des droits de l'homme, les journalistes d'investigation et les activistes. Qurium propose un éventail de solutions sécurisées,  professionnelles et personnalisées avec une assistance aux organisations et personnes à risque, incluant:

- Hébergement sécurisé avec protection DDoS de sites à risque
- Assistance rapide aux organisations et personnes en danger imminent
- Audits de sécurité des services web et des applications mobiles
- Contournement des blocages de sites web
- Investigations forensiques d'attaques numériques, d'applications frauduleuses, de malwares ciblés et des désinformations