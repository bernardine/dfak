---
layout: page
title: "Êtes-vous victime de harcèlement en ligne ?"
author: Floriana Pagano
language: fr
summary: "Êtes-vous victime de harcèlement en ligne ?"
date: 2019-04-01
permalink: /fr/topics/harassed-online/
parent: /fr/
---

# Are you being targeted by online harassment?

Internet, et les plateformes de médias sociaux en particulier, sont devenus un espace vital pour les membres et les organisations de la société civile, en particulier pour les femmes, les personnes LGBTIQ et les autres minorités, afin qu'ils puissent s'exprimer et faire entendre leur voix. Mais en même temps, ils sont aussi devenus des espaces où ces groupes sont facilement ciblés pour le fait d'exprimer leurs points de vue. La violence et les abus en ligne privent les femmes, les personnes LGBTIQ et de nombreuses autres personnes défavorisées du droit de s'exprimer de manière égale, libre et sans crainte.

La violence et les abus en ligne revêtent de nombreuses formes différentes et les auteurs peuvent souvent compter sur l'impunité, également en raison de l'absence de lois protégeant les victimes de harcèlement dans de nombreux pays, mais surtout parce que les stratégies de protection doivent être modifiées de manière créative en fonction du type d'attaque qui est lancé.

Il est donc important d'identifier la typologie de l'attaque qui nous vise pour décider des mesures que nous pouvons prendre.

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour planifier la façon de vous protéger contre l'attaque dont vous souffrez.

Si vous êtes victime de harcèlement en ligne, suivez ce questionnaire pour identifier la nature de votre problème et trouver de possibles solutions.

## Workflow

### physical_wellbeing

Craignez-vous pour votre intégrité physique ou votre bien-être ?

- [Oui](#physical_risk_end)
- [Non](#no_physical_risk)


### no_physical_risk

Pensez-vous que l'attaquant a accédé ou est en train d'accéder à votre appareil ?

 - [Oui](#device_compromised)
 - [Non](#account_compromised)

### device_compromised

> Modifiez le mot de passe pour accéder à votre appareil pour un mot de passe unique, long et complexe :
>
> [Mac OS](https://support.apple.com/fr-fr/HT202860)
> [Windows](https://support.microsoft.com/fr-fr/help/4490115/windows-change-or-reset-your-password)
> [iOS - Apple ID](https://support.apple.com/fr-fr/HT201355)
> [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en=GENIE.Platform%3DAndroid&hl=fr)

L'attaquant a-t-il effectivement réussi à verrouiller votre appareil ?

 - [Oui](#account_compromised)
 - [Non](../../../device-acting-suspiciously)


### account_compromised

> Si quelqu'un a eu accès à votre appareil, il peut également avoir accédé à vos comptes en ligne, de sorte qu'il puisse lire vos messages privés, identifier vos contacts et publier des messages, des images ou des vidéos en se faisant passer pour vous.

Avez-vous remarqué la disparition de messages, ou d'autres activités qui vous donnent de bonnes raisons de penser que votre compte a pu être compromis ? Vérifiez également votre dossier d'envoi pour détecter toute activité suspecte.

 - [Oui](../../../account-access-issues)
 - [Non](#impersonation)

### impersonation

Quelqu'un se fait-il passer pour vous ?

- [Oui](../../../impersonated)
- [Non](#doxing)

### doxing

Quelqu'un a-t-il publié des informations personnelles ou des photos sans votre consentement ?

- [Oui](#doxing_yes)
- [Non](#hate_speech)

### doxing_yes

Où vos informations personnelles ou vos photos ont-elles été publiées ?

- [Sur une plate-forme de réseautage social](#doxing_sn)
- [Sur un site Web](#doxing_web)

### doxing_sn

> Si vos informations personnelles ou vos photos ont été publiées sur un réseau social, vous pouvez signaler une violation des règles communautaires en suivant les procédures de signalement fournies aux utilisateurs par les sites de réseaux sociaux. Vous trouverez les instructions pour les principales plates-formes dans la liste suivante :
>
> - [Google](https://www.cybercivilrights.org/online-removal/#google)
> - [Facebook](https://www.cybercivilrights.org/online-removal/#facebook)
> - [Twitter](https://www.cybercivilrights.org/online-removal/#twitter)
> - [Tumblr](https://www.cybercivilrights.org/online-removal/#tumblr)
> - [Instagram](https://www.cybercivilrights.org/online-removal/#instagram)

L'information ou les médias ont-ils été supprimés ?

 - [Oui](#one_more_persons)
 - [Non](#harassment_end)

### doxing_web

> Suivez [ces instructions](https://withoutmyconsent.org/resources/take-down) pour retirer le contenu d'un site Web.

Le contenu a-t-il été retiré par le site Web ?

- [Oui](#one_more_persons)
- [Non](#harassment_end)


### hate_speech

L'attaque est-elle fondée sur des attributs comme la race, le sexe ou la religion ?

- [Oui](#one_more_persons)
- [Non](#harassment_end)


### one_more_persons

Avez-vous été attaqué par une ou plusieurs personnes ?

- [Une personne](#one_person)
- [Plusieurs personnes](#more_persons)

### one_person

Connaissez-vous cette personne ?

- [Oui](#known_harasser)
- [Non](#block_harasser)


### known_harasser

> Si vous savez qui vous harcèle, vous pouvez envisager de le signaler aux autorités de votre pays. Chaque pays a des lois différentes pour protéger les personnes contre le harcèlement en ligne, et vous devriez explorer la législation de votre pays pour décider quoi faire.
>
> Si vous décidez de poursuivre cette personne en justice, vous devriez vous adresser à un expert juridique.

Vous voulez poursuivre l'agresseur ?

 - [Oui](#legal_end)
 - [Non](#block_harasser)


### block_harasser

> Que vous sachiez qui est votre harceleur ou non, c'est toujours une bonne idée de les bloquer sur les plateformes de réseautage social dans la mesure du possible.
>
> - [Facebook](https://www.facebook.com/help/290450221052800)
> - [Twitter](https://help.twitter.com/fr/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=fr)
> - [Tumblr](https://tumblr.zendesk.com/hc/fr/articles/231877648-Blocking-users)
> - [Instagram](https://help.instagram.com/426700567389543)

Avez-vous bloqué votre harceleur efficacement ?

 - [Oui](#resolved_end)
 - [Non](#harassment_end)


### more_persons

> Si vous êtes attaqué par plus d'une personne, vous pourriez être la cible d'une campagne de harcèlement, et vous devrez réfléchir à la meilleure stratégie qui s'applique à votre cas.
>
> Pour en savoir plus sur toutes les stratégies possibles, lisez cette [page](https://www.takebackthetech.net/be-safe/hate-speech-strategies)

Avez-vous identifié la meilleure stratégie pour vous ?

 - [Oui](#resolved_end)
 - [Non](#harassment_end)


### harassment_end

> Si vous êtes toujours victime de harcèlement et que vous avez besoin d'une solution personnalisée, veuillez communiquer avec les organismes ci-dessous qui peuvent vous aider.

:[](organisations?services=harassment)


### physical_risk_end

> Si vous courez un risque physique, veuillez entrer en contact avec l'un de ces organismes qui peuvent vous aider.

:[](organisations?services=physical_security)


### legal_end

> Si vous avez besoin d'un soutien juridique, veuillez centrer en contact avec les organismes ci-dessous qui peuvent vous aider.

:[](organisations?services=legal)


### resolved_end

Nous espérons que ce guide de dépannage vous a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)


### final_tips

- **Documenter le harcèlement:** Il est utile de documenter les attaques ou tout autre incident dont vous pouvez être témoin : prenez des captures d'écran, sauvegardez les messages que vous recevez des harceleurs, etc. Si possible, créez un journal dans lequel vous pouvez systématiser cette documentation en enregistrant les dates, heures, plates-formes et lieux en ligne, ID utilisateur, captures d'écran, description des événements, etc. Les journaux peuvent vous aider à détecter d'éventuels modèles et des indications sur vos agresseurs potentiels. Si vous vous sentez dépassé, essayez de penser à quelqu'un en qui vous avez confiance et qui pourrait documenter les incidents pour vous pendant un certain temps. Vous devez faire particulièrement confiance à la personne qui gérera cette documentation, car vous devrez lui remettre les informations d'identification de vos comptes personnels. Une fois que vous sentez que vous pouvez reprendre le contrôle de vos comptes, n'oubliez pas de changer vos mots de passe.

    - Vous trouverez des instructions sur la façon de documenter l'incident dans [cette page](https://www.techsafety.org/documentationtips/).

- **Configurez l'authentification à 2 facteurs** sur tous vos comptes. L'authentification à 2 facteurs peut être très efficace pour empêcher quelqu'un d'accéder à vos comptes sans votre permission. Si vous pouvez choisir, n'utilisez pas l'authentification par SMS à 2 facteurs et choisissez une autre option, basée sur une application téléphonique ou sur une clé de sécurité.

    - Si vous ne savez pas quelle solution est la meilleure pour vous, vous pouvez consulter [cet infographie](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) et [cet article](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - Vous trouverez des instructions pour configurer l'authentification à 2 facteurs sur les principales plates-formes [ici] (https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Cartographie de votre présence en ligne**. L'auto-espionnage consiste à explorer les informations librement disponibles sur soi-même pour empêcher des acteurs malveillants de trouver et d'utiliser ces informations pour se faire passer pour vous.


#### Resources

- [Access Now Helpline Community Documentation: Un guide pour prévenir la divulgation d'informations sensibles](https://guides.accessnow.org/self-doxing/self-doxing.html)
- [Access Now Helpline Community Documentation: FAQ - Online Harassment Targeting a Civil Society Member](https://accessnowhelpline.gitlab.io/community-documentation/234-FAQ-Online_Harassment.html)​​​​​​​
- [Equality Labs: Anti-doxing guide pour les activistes qui font face à des attaques de l'extrême droite](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Verrouiller votre identité numérique](http://femtechnet.org/csov/lock-down-your-digital-identity/)
- [National Network to End Domestic Violence: Conseils de documentation pour les survivants d'abus et de harcèlement technologique](https://www.techsafety.org/documentationtips)
