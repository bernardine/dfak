---
layout: page
title: "Formulaire de contact"
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2018-09
permalink: /fr/contact-methods/contact-form.md
parent: /fr/
published: true
---

Un formulaire de contact protégera très probablement votre échange avec l'organisme destinataire afin que vous et l'organisme destinataire soyez les seuls à pouvoir le lire. Toutefois, le fait que vous ayez communiqué avec l'organisme peut être accessible aux gouvernements, aux organismes d'application de la loi ou à d'autres parties ayant l'équipement technique nécessaire. Si vous souhaitez mieux protéger le fait que vous vous adressez à cette organisation, utilisez le navigateur Tor pour accéder au site web et au formulaire de contact.

