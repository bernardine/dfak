---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: ativistas, jornalistas, ddhs, lgbti, mulheres, jovens, tsd
hours: Segunda-Quinta 9:00-17:00 CET
response_time: 4 dias
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

A Digital Defenders Partnership (DDP) foca em proteger e avançar na liberdade da internet, e em manter a internet livre de ameaças que a tornem um ambiente de repressão. Coordenamos suporte emergencial a indivíduos e organizações, como defensores de direitos humanos, jornalistas, ativistas da sociedade civil, e produtoras de conteúdo. Temos uma abordagem voltada a pessoas, focando nossos valores em transparência, direitos humanos, inclusividade e diversidade, equidade, confidencialidade, e liberdade. DDP foi formada em 2012 pela Freedom Online Coalition (FOC).

A DDP possui três tipos diferentes de fundos emergenciais, assim como grants de longa duração focados em desenvolver capacidades com organizações. Além disso, coordenamos a Digital Integrity Fellowship onde organizações recebem treinamentos personalizados em segurança digital e privacidade, e um programa de Rede de Resposta Rápida (RaReNet).
