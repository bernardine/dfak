---
layout: page
title: PGP
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/pgp.md
parent: /pt/
published: true
---

PGP (ou Pretty Good Privacy) e seu equivalente de código aberto, GPG (Gnu Privacy Guard), permitem que você criptografe o conteúdo dos emails para proteger suas mensagens de serem vistas por provedores de email ou outros atores que possam acessar a mensagem. No entanto, porém, governos e autoridades na sua jurisdição e da pessoa receptora da mensagem estão respaldadas a serem notificadas sobre as suas comunicações. Para prevenir isto, você pode criar uma conta alternativa que não esteja associada com a sua identidade.

Resources: [Access Now Helpline Community Documentation: Secure Email](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Freedom of the Press Foundation: Encrypting Email with Mailvelope: A Beginner's Guide](https://freedom.press/training/encrypting-email-mailvelope-guide/)

[Privacy Tools: Provedores de Email com Privacidade](https://www.privacidade.digital/provedores/email/)
