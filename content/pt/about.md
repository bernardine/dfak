---
layout: page.pug
title: "Sobre"
language: pt
summary: "Sobre o Kit de Primeiros Socorros Digitais."
date: 2019-03-13
permalink: /pt/about/
parent: Home
---

O Kit de Primeiros Socorros Digitais é um esforço colaborativo da [RaReNet (Rapid Response Network)](https://www.rarenet.org/) e da [CiviCERT](https://www.civicert.org/).

A Rede de Pronta Resposta (Rapid Reponse Network) é uma rede internacional de organizações dedicadas à resposta rápida de incidentes e de referências em segurança digital que inclui: Access Now,  Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtual Road e também especialistas em segurança que trabalham no campo da segurança digital e da pronta resposta.

Algumas destas organizações e indivíduos fazem parte da CiviCERT, uma rede internacional de provedoras de infraestrutura e atendimento em segurança digital que foca principalmente no apoio a grupos e organizações lutando em favor da justiça social e defesa de direitos humanos e digitais. A CiviCERT reune diferentes comunidades de times de resposta a emergências digitais (conhecidos como CERT), e é certificada pela Trusted Introducer, rede européia de certificação de comunidades CERT.

O Kit de Primeiros Socorros Digitais é [um projeto que aceita contribuições de pessoas interessadas de maneira livre e que possui código aberto.](https://gitlab.com/rarenet/dfak/pt)

Se você quiser usar o Kit de Primeiros Socorros Digitais em contextos onde a conectividade é limitada, ou encontrar uma conexão é difícil, você pode [baixar uma versão offline aqui](https://www.digitalfirstaid.org/dfak-offline.zip).

Para qualquer comentário, sugestão ou pergunta sobre o Kit de Primeiros Socorros Digitais, você pode escrever para: dfak @ digitaldefenders . org

GPG - Impressão digital: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
