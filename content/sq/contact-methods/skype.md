---
layout: page
title: Skype
author: mfc, Metamorphosis Foundation
language: sq
summary: Metodat e kontaktit
date: 2018-09
permalink: /sq/contact-methods/skype.md
parent: /sq/
published: true
---

Përmbajtja e mesazhit tuaj si dhe fakti që keni kontaktuar me organizatën mund të jetë i arritshëm nga qeveritë, organet e zbatimit të ligjit.
